# Feuille de note

## Présentation

Ce dépôt contient principalement la fichier `note.cls`, définissant la classe "note" permettant de créer simplement des feuilles de note.

## Utilisation

Pour créer une nouvelle feuille de note, il suffit de placer le fichier `note.cls` dans un dossier, de créer un nouveau document .tex dans ce même dossier, puis d'y écrire :

```latex
\documentclass{note}


\title{...}
\nbpages{...}

\begin{document}

    \colonne{...}
    \colonne{...}
    ...

    \makeall

\end{document}
```

en remplaçant les ... comme suit :

 * `\title` définit le titre en haut des différentes pages
 * `\nbpages` définit le nombre total de pages à créer
 * `\colonne` permet d'ajouter une nouvelle colonne à la feuille de note. Cela peut être pratique pour mettre les différentes options s'il y a lieu.
 * `\makeall` permet d'afficher le résultat final.

Un exemple mettant cela en pratique est disponible dans ce même dépôt.

Il ne reste alors plus qu'à compiler le document .tex.

PS : Ne pas tenir compte des warnings ^^'