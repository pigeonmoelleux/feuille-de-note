\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{note}[2022/10/20]
\LoadClass[a4paper, 12pt]{article}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[french]{babel}

\RequirePackage{xparse}
\RequirePackage{geometry}
\newgeometry{top=70pt, bottom=100pt, left=20pt, right=50pt}

\renewcommand{\title}[1]{
    \def\@title{#1}
}

\RequirePackage{fancyhdr}
\setlength{\headheight}{25pt}
\fancyhead[L]{}
\fancyhead[C]{\Huge \bfseries \@title}
\fancyhead[R]{\LARGE Page \fbox{\thepage}}
\fancyfoot[L,C,R]{}
\pagestyle{fancy}
\renewcommand{\headrule}{}

\RequirePackage{tabularx}
\newcolumntype{Y}{>{\centering\arraybackslash}X}

\renewcommand{\arraystretch}{2.7}

\ExplSyntaxOn

\NewDocumentCommand{\colonne}{ m }{ \note_input_add:n { #1 } }
\NewDocumentCommand{\makeall}{}{ \note_makeall: }
\NewDocumentCommand{\nbpages}{ m }{ \note_nbpages_int:n { #1 } }

\seq_new:N \g_note_input_seq
\int_new:N \l_note_counter_int
\int_new:N \g_note_nbpages_int

\cs_new:Npn \note_input_add:n #1
    {
        \int_incr:N \l_note_counter_int
        \seq_gput_right:Nn \g_note_input_seq { #1 }
    }

\cs_new:Npn \note_maketab: 
    {
        \tl_set:Nn \l_tab_tl { \begin{tabularx}{\textwidth}{|Y|*{\l_note_counter_int}{Y|}} }

        \tl_put_right:Nn \l_tab_tl { \hline \textsf{\l_note_first_col_str} }

        \tl_put_right:Nn \l_tab_tl { \seq_map_inline:Nn \g_note_input_seq { & \textsf{##1} } }

        \tl_put_right:Nn \l_tab_tl { \\ \hline }

        \tl_put_right:Nn \l_tab_tl { \int_step_inline:nn {15} { \int_step_inline:nn { \l_note_counter_int } { & } \\ \hline } }

        \tl_put_right:Nn \l_tab_tl { \int_step_inline:nn { \l_note_counter_int } { & } \\ \hline \end{tabularx} }

        \tl_use:N \l_tab_tl
    }

\cs_new:Npn \note_nbpages_int:n #1
    {
        \int_set:Nn \g_note_nbpages_int { #1 }
    }

\cs_new:Npn \note_makeall:
    {
        \int_decr:N \l_note_counter_int

        \str_new:N \l_note_first_col_str
        \seq_pop_left:NN { \g_note_input_seq } { \l_note_first_col_str }

        \int_step_inline:nn { \g_note_nbpages_int } { \note_maketab: \newpage }
    }

\ExplSyntaxOff

\colonne{Note}
